package com.iralauf.investment.coravita.views.submodules;

import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import com.iralauf.investment.coravita.R;
import com.iralauf.investment.coravita.backgroundtask.ActiveModeScanningAlarmManager;
import com.iralauf.investment.coravita.backgroundtask.ActiveModeScanningService;
import com.iralauf.investment.coravita.databinding.ActivitySettingsBinding;
import com.iralauf.investment.coravita.persistence.AppDatabaseClient;
import com.iralauf.investment.coravita.persistence.WorldData.WorldData;
import com.iralauf.investment.coravita.utils.Common;
import com.iralauf.investment.coravita.utils.SharedPrefUtils;

import static com.iralauf.investment.coravita.utils.Common.errorLogger;
import static com.iralauf.investment.coravita.utils.Common.getDate;

public class AppSettingsActivity extends AppCompatActivity {

    public static final String TAG = AppSettingsActivity.class.getSimpleName();
    ActivitySettingsBinding activitySettingsBinding;
    private static final String KEY_CALCULATED_PREVALENCE="calculated_prevalence";
    private static final String KEY_COUNTRY="country";
    private static final String KEY_DATETIME="datetime";

    //**********************************************************************************************
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            activitySettingsBinding = ActivitySettingsBinding.inflate(getLayoutInflater());
            View view = activitySettingsBinding.getRoot();
            setContentView(view);
            setUpActionBar();
            setInit();
        } catch (Exception e) {
            errorLogger(e, "onCreate: ", TAG);
        }
    }

    private ProgressDialog dialog;

    private void setInit() {

        activitySettingsBinding.txtBluetoothscaninterval.setText(String.format(getString(R.string.bluetoothscanning_interval), Common.convertMillisecondsToMinutes(ActiveModeScanningAlarmManager.alarmExecuteInterval)));

        if(activitySettingsBinding.notificationControl.isChecked()){
            activitySettingsBinding.notificationSelection.setVisibility(View.VISIBLE);
        }

        activitySettingsBinding.notificationControl.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                activitySettingsBinding.notificationSelection.setVisibility(View.VISIBLE);
            }else{
                activitySettingsBinding.notificationSelection.setVisibility(View.GONE);
            }
        });

        refreshContent();

        activitySettingsBinding.forcerefresh.setOnClickListener(v -> {
            ActiveModeScanningService.getWorldInData();
            dialog = new ProgressDialog(this);
            dialog.setMessage(getString(R.string.waitmoment));
            dialog.setCancelable(false);
            dialog.show();
            updateContentLoader();
        });


        activitySettingsBinding.radiogrp1.setOnCheckedChangeListener(listener1);
        activitySettingsBinding.radiogrp2.setOnCheckedChangeListener(listener2);

    }

    private final RadioGroup.OnCheckedChangeListener listener1 = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId != -1) {
                activitySettingsBinding.radiogrp2.setOnCheckedChangeListener(null);
                activitySettingsBinding.radiogrp2.clearCheck();
                activitySettingsBinding.radiogrp2.setOnCheckedChangeListener(listener2);
                Log.e("XXX2", "do the work");

            }
        }
    };

    private final RadioGroup.OnCheckedChangeListener listener2 = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId != -1) {
                activitySettingsBinding.radiogrp1.setOnCheckedChangeListener(null);
                activitySettingsBinding.radiogrp1.clearCheck();
                activitySettingsBinding.radiogrp1.setOnCheckedChangeListener(listener1);
                Log.e("XXX2", "do the work");
            }
        }
    };

    private static final int MENU_ITEM_ITEM1 = 1;

    void updateContentLoader(){

        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(() -> {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            refreshContent();
            Toast.makeText(getApplicationContext(), R.string.updated, Toast.LENGTH_LONG).show();
        }, 10000);

    }

    private void refreshContent(){
        Handler handler = new Handler(Looper.myLooper()) {
            @Override
            public void handleMessage(Message msg) {
                Bundle bundle = msg.getData();
                activitySettingsBinding.txtPrevalencecalculation.setText(bundle.getString(KEY_CALCULATED_PREVALENCE));
                activitySettingsBinding.txtPrevalencedatainfo.setText(String.format(getString(R.string.prevalencedatainfo), bundle.getString(KEY_COUNTRY), bundle.getString(KEY_DATETIME)));
            }
        };

        Runnable runnable = () -> {
            Message msg = handler.obtainMessage();
            WorldData prevalenceResults= AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().worldDataDao().getCurrentWorldData(getDate());
            if(prevalenceResults!=null) {
                Bundle bundle = new Bundle();
                bundle.putString(KEY_CALCULATED_PREVALENCE, prevalenceResults.getCalculatedPrevalence());
                bundle.putString(KEY_COUNTRY, prevalenceResults.getCountryName());
                bundle.putString(KEY_DATETIME, Common.getDateString(prevalenceResults.getCreatedAt()));
                msg.setData(bundle);
                handler.sendMessage(msg);
            }
        };
        Thread riskThread = new Thread(runnable);
        riskThread.start();

    }

    private void setUpActionBar() {
        setUpTheme();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.appsettings_activity_title));
    }
    //**********************************************************************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    //**********************************************************************************************
    public void setUpTheme() {
        SharedPrefUtils sharedPrefUtils = SharedPrefUtils.getInstance(this);
        if (sharedPrefUtils.isDarkMode()) {
            setTheme(R.style.AppTheme_Dark);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorthemeDarkPrimary)));
        } else {
            setTheme(R.style.AppTheme_Light);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorthemeLightPrimary)));
        }
    }
    //**********************************************************************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.FIRST, MENU_ITEM_ITEM1, Menu.NONE, "Save").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_ITEM_ITEM1:
               // saveSettings();
                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return false;
        }
    }

    private void saveSettings() {

        int selectedRadioButtonId = activitySettingsBinding.radiogrp2.getCheckedRadioButtonId();
        RadioButton radioButton = findViewById(selectedRadioButtonId);
        String selectedRbText = radioButton.getText().toString();
        Log.e(selectedRbText, " is Selected");

        selectedRadioButtonId = activitySettingsBinding.radiogrp1.getCheckedRadioButtonId();
        radioButton = findViewById(selectedRadioButtonId);
        selectedRbText = radioButton.getText().toString();
        Log.e(selectedRbText, " is Selected");

        //check is enabled

        //save the settings

    }
    //**********************************************************************************************

}
