package com.iralauf.investment.coravita.backgroundtask.workers;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.iralauf.investment.coravita.persistence.AppDatabaseClient;
import com.iralauf.investment.coravita.persistence.WorldData.WorldData;
import com.iralauf.investment.coravita.utils.Common;
import com.iralauf.investment.coravita.utils.SharedPrefUtils;
import com.iralauf.investment.coravita.views.supportedmodules.ourworldindata.OurWorldInData;
import com.iralauf.investment.coravita.views.supportedmodules.ourworldindata.OurWorldInDataService;
import com.iralauf.investment.coravita.views.supportedmodules.prevalencecalculation.PrevalenceCalculation;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.iralauf.investment.coravita.utils.Common.errorLogger;
import static com.iralauf.investment.coravita.utils.Common.getCountryCodeFromCountry;
import static com.iralauf.investment.coravita.utils.Common.getCurrentDateTime;

public class GetWorldInDataWorker extends Worker {

    SharedPrefUtils sharedPrefUtils;
    Context context;
    String TAG = GetWorldInDataWorker.class.getSimpleName();
    String worldURL = "https://covid.ourworldindata.org";

    public GetWorldInDataWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.sharedPrefUtils = new SharedPrefUtils(context);
        this.context = context;
    }

    @Override
    public Result doWork() {

        String countryCode = getCountryCodeFromCountry(sharedPrefUtils.getCurrentCountry());

        Log.e(TAG, "getWorldInData-->" + countryCode);

        if (countryCode != null) {

            try {
                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(worldURL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(httpClient.build())
                        .build();

                OurWorldInDataService service = retrofit.create(OurWorldInDataService.class);
                Call<JsonObject> call = service.getWorldInData();
                Response<JsonObject> response = call.execute();

                if(response!=null){
                    Log.e(TAG, "Deleted all");
                    AppDatabaseClient.getInstance(context).getAppDatabase().worldDataDao().deleteAll();
                    JsonObject jsonObject = response.body();
                    ObjectMapper objectMapper = new ObjectMapper();
                    OurWorldInData ourWorldInData = null;
                    try {
                        ourWorldInData = objectMapper.readValue(jsonObject.getAsJsonObject(countryCode).toString(), OurWorldInData.class);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    Log.e(TAG, ourWorldInData.getContinent() + "-->" + ourWorldInData);
                    WorldData worldData = new WorldData();
                    worldData.setActive(true);
                    worldData.setCountryCode(countryCode);
                    worldData.setCreatedAt(getCurrentDateTime());
                    worldData.setLastRetrievedDate(Common.getDate());
                    worldData.setWorldDataJson(jsonObject.getAsJsonObject(countryCode).toString());
                    worldData.setCountryName(sharedPrefUtils.getCurrentCountry());
                    AppDatabaseClient.getInstance(context).getAppDatabase().worldDataDao().insert(worldData);
                    PrevalenceCalculation prevalenceCalculation = new PrevalenceCalculation();
                    prevalenceCalculation.CalculatePrevalence(ourWorldInData);

                }else{
                    //TODO show alert to user
                    return Result.failure();
                }

            } catch (Exception e) {
                errorLogger(e, "getWorldInData", context.getClass().getSimpleName());
            }
        }
        return Result.success();
    }
}
