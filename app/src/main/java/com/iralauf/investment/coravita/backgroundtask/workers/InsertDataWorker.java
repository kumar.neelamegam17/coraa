package com.iralauf.investment.coravita.backgroundtask.workers;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.iralauf.investment.coravita.models.ScanBluetooth;
import com.iralauf.investment.coravita.models.ScanStatus;
import com.iralauf.investment.coravita.persistence.AppDatabaseClient;
import com.iralauf.investment.coravita.persistence.ScanData.ScanObject;
import com.iralauf.investment.coravita.persistence.ScanResults.ScanResultsObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.iralauf.investment.coravita.utils.Common.KEY_COUNTERTAG;
import static com.iralauf.investment.coravita.utils.Common.KEY_LATITUDE;
import static com.iralauf.investment.coravita.utils.Common.KEY_LONGITUDE;
import static com.iralauf.investment.coravita.utils.Common.KEY_SCANNED_DATA;
import static com.iralauf.investment.coravita.utils.Common.errorLogger;
import static com.iralauf.investment.coravita.utils.Common.getCurrentDateTime;
import static com.iralauf.investment.coravita.utils.Common.getDevTypeDescription;
import static com.iralauf.investment.coravita.utils.Common.getDeviceMajorClass;
import static com.iralauf.investment.coravita.utils.Common.getDistance;

public class InsertDataWorker extends Worker {

    String TAG=InsertDataWorker.class.getSimpleName();
    Context context;
    public InsertDataWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context=context;
    }

    @NonNull
    @Override
    public Result doWork() {

        try {
            double latitude=getInputData().getDouble(KEY_LATITUDE, 0);
            double longitude=getInputData().getDouble(KEY_LONGITUDE, 0);
            int countTag=getInputData().getInt(KEY_COUNTERTAG, 0);
            String scannedListJson=getInputData().getString(KEY_SCANNED_DATA);

            Type listType = new TypeToken<List<ScanBluetooth>>() {}.getType();
            List<ScanBluetooth> scannedDevicesList = new Gson().fromJson(scannedListJson , listType);
            List<ScanObject> scanObjectArrayList = new ArrayList<>();

            if(scannedDevicesList!=null && scannedDevicesList.size()>0){
                for (ScanBluetooth scanBluetooth : scannedDevicesList) {
                    BluetoothDevice bluetoothDevice = scanBluetooth.getBluetoothDevice();
                    String rssi = scanBluetooth.getRssi();
                    ScanObject scanObject = new ScanObject();
                    scanObject.setCreatedAt(getCurrentDateTime());
                    scanObject.setSignalStrength(rssi);
                    scanObject.setRssi(rssi);
                    scanObject.setDistance(getDistance(Integer.parseInt(rssi)));
                    scanObject.setScanStatus(ScanStatus.SCANNED.name());
                    scanObject.setLatitude(latitude);
                    scanObject.setLongitude(longitude);
                    scanObject.setDeviceType(getDeviceMajorClass(bluetoothDevice.getBluetoothClass().getMajorDeviceClass()));
                    scanObject.setBluetooth_type(getDevTypeDescription(bluetoothDevice.getType()));
                    scanObject.setMacAddress(bluetoothDevice.getAddress());
                    scanObject.setActive(true);
                    scanObject.setCountTag(countTag);
                    AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().scanDao().insert(scanObject);
                    scanObjectArrayList.add(scanObject);
                }
            }else{
                errorLogger(null, "No device found", getApplicationContext().getClass().getSimpleName());
                // insert empty scan
                ScanObject scanObject = new ScanObject();
                scanObject.setCreatedAt(getCurrentDateTime());
                scanObject.setScanStatus(ScanStatus.SCANNED.name());
                scanObject.setLatitude(latitude);
                scanObject.setLongitude(longitude);
                scanObject.setActive(true);
                scanObject.setCountTag(countTag);
                AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().scanDao().insert(scanObject);
            }

            if(scanObjectArrayList!=null && scanObjectArrayList.size()>0){
                //insert into scanresults
                int currentDeviceCount=AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().scanDao().getDeviceCount(countTag);
                Gson gson = new Gson();
                String json = gson.toJson(scanObjectArrayList);
                ScanResultsObject scanResults=new ScanResultsObject();
                scanResults.setActive(true);
                scanResults.setCreatedAt(getCurrentDateTime());
                scanResults.setDeviceCount(currentDeviceCount);
                scanResults.setDataDump(json);
                Log.e(TAG, "currentDeviceCount: "+ currentDeviceCount);
                Log.e(TAG, "countTag: "+ countTag);
                AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().scanResultsDao().insert(scanResults);
            }

        } catch (Exception e) {
            errorLogger(e, "InsertDataWorker", getApplicationContext().getClass().getSimpleName());
        }
        return Result.success();
    }
}
