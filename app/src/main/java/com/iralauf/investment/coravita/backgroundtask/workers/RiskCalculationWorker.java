package com.iralauf.investment.coravita.backgroundtask.workers;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.iralauf.investment.coravita.R;
import com.iralauf.investment.coravita.persistence.AppDatabaseClient;
import com.iralauf.investment.coravita.persistence.WorldData.WorldData;
import com.iralauf.investment.coravita.utils.Common;
import com.iralauf.investment.coravita.views.supportedmodules.riskcalculation.RiskCalculation;

import static com.iralauf.investment.coravita.utils.Common.KEY_MINIMUMDEVICE_COUNT;
import static com.iralauf.investment.coravita.utils.Common.getDate;

public class RiskCalculationWorker extends Worker {
    Context context;
    String TAG = RiskCalculationWorker.class.getSimpleName();

    public RiskCalculationWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
    }

    @NonNull
    @Override
    public Result doWork() {

        int minimumDeviceCount=getInputData().getInt(KEY_MINIMUMDEVICE_COUNT,3);
        int deviceCountOfLastScanCycle= AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().scanDao().getTotalDeviceCountInLastScan(minimumDeviceCount);
        Log.e(TAG, "deviceCountOfLastScanCycle: "+ deviceCountOfLastScanCycle);
        Log.e(TAG, "deviceCountOfLastScanCycle: "+ deviceCountOfLastScanCycle);
        AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().scanDao().updateAllActiveStatus();

        WorldData worldData=AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().worldDataDao().getCurrentWorldData(getDate());
        String currentPrevalence=worldData.getCalculatedPrevalence();
        Log.e(TAG, "getCalculatedPrevalence: "+ currentPrevalence);

        RiskCalculation riskCalculation=new RiskCalculation();
        riskCalculation.RiskCalculation(context);
        String value=riskCalculation.CalculateRisk(Double.parseDouble(currentPrevalence), deviceCountOfLastScanCycle);

        callNotification(context, value);

        return Result.success();
    }


    private void callNotification(Context context, String riskValue) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "my_channel_id_2020";
        String title=context.getString(R.string.app_name);
        String description=getNotificationInfo(Common.convertRiskDecimals(Double.parseDouble(riskValue)), context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, title, NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription(description);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID);
        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_baseline_coronavirus_24)
                .setTicker(title)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentTitle(title)
                .setContentText(description)
                .setContentInfo(context.getString(R.string.info));
        notificationManager.notify(/*notification id*/1588, notificationBuilder.build());
    }

    String getNotificationInfo(String value, Context context){
        double riskValue=Double.parseDouble(value);
        if(riskValue>=0.0 && riskValue<=0.2){
            return "Risk calculated during last hour is: "+riskValue+"\nRisk Status: "+context.getText(R.string.low_risk_sl);
        }else if(riskValue>=0.2 && riskValue<=0.5){
            return "Risk calculated during last hour is: "+riskValue+"\nRisk Status: "+context.getText(R.string.moderate_risk_sl);
        }else if(riskValue>=0.5 && riskValue<=0.9){
            return "Risk calculated during last hour is: "+riskValue+"\nRisk Status: "+context.getText(R.string.high_risk_sl);
        }else if(riskValue>=0.9 && riskValue<=1.0){
            return "Risk calculated during last hour is: "+riskValue+"\nRisk Status: "+context.getText(R.string.veryhigh_risk_sl);
        }
        return "Risk calculated during last hour is "+riskValue;
    }

}
