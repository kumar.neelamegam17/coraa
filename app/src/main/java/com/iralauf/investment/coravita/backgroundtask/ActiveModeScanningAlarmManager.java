package com.iralauf.investment.coravita.backgroundtask;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.iralauf.investment.coravita.utils.SharedPrefUtils;

import static android.content.Context.ALARM_SERVICE;

public class ActiveModeScanningAlarmManager implements ActiveModeScanningIF {

    String TAG = ActiveModeScanningAlarmManager.class.getSimpleName();
    PendingIntent pendingIntent;
    AlarmManager alarmManager;
    Context context;
    public static long alarmExecuteInterval=300000;//300000ms to every 5 mins
    SharedPrefUtils sharedPrefUtils;

    @Override
    public void initActiveMode(Context context) {
        this.context=context;
        sharedPrefUtils=new SharedPrefUtils(context);
        alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(context, ActiveModeScanningService.class);
        pendingIntent = PendingIntent.getService(context, 0, intent, 0);
        Log.e(TAG, "initActiveMode");
    }

    @Override
    public void startActiveMode() {
        if (alarmManager != null) {
            if(sharedPrefUtils.getScanningMode()){
                if (!isMyServiceRunning(ActiveModeScanningService.class, context)) {
                    long alarmStartTime = System.currentTimeMillis();
                    Log.e(TAG, "Interval-" + alarmExecuteInterval);
                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmStartTime, alarmExecuteInterval, pendingIntent);
                    Log.e(TAG, "Installed SDK:"+Build.VERSION.SDK_INT);
                    Log.e(TAG, "startActiveMode");
                }
            }
        }
    }

    @Override
    public void stopActiveMode() {
        if (alarmManager != null) {
            if(!sharedPrefUtils.getScanningMode()){
                if (isMyServiceRunning(ActiveModeScanningService.class, context)) {
                    pendingIntent.cancel();
                    alarmManager.cancel(pendingIntent);
                    Log.e(TAG, "stopActiveMode");
                }
            }
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
