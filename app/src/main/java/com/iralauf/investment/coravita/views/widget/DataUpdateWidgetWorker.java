package com.iralauf.investment.coravita.views.widget;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.iralauf.investment.coravita.persistence.AppDatabaseClient;
import com.iralauf.investment.coravita.utils.Common;
import com.iralauf.investment.coravita.utils.SharedPrefUtils;

public class DataUpdateWidgetWorker extends Worker {
    private final Context context;
    SharedPrefUtils sharedPrefUtils;

    public DataUpdateWidgetWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
        sharedPrefUtils=new SharedPrefUtils(context);
    }

    @NonNull
    @Override
    public Result doWork() {
        try {
            Double riskValue = AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().riskDao().getLastRiskValue();
            if (riskValue != null) {
                sharedPrefUtils.setLastRiskValue(Common.convertRiskDecimals(riskValue));
                updateWidget();
                Log.d("DataUpdateWidgetWorker", "Work is Done");
                return Result.success();
            } else {
                return Result.failure();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.failure();
    }

    private void updateWidget() {
        Intent intent = new Intent(context, Widget.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        // Use an array and EXTRA_APPWIDGET_IDS instead of AppWidgetManager.EXTRA_APPWIDGET_ID,
        // since it seems the onUpdate() is only fired on that:
        int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, Widget.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        context.sendBroadcast(intent);
    }
}
