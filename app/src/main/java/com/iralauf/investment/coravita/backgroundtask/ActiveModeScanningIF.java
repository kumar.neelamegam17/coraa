package com.iralauf.investment.coravita.backgroundtask;

import android.content.Context;

public interface ActiveModeScanningIF {
    void initActiveMode(Context context);

    void startActiveMode();

    void stopActiveMode();
}
