package com.iralauf.investment.coravita.backgroundtask;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.google.gson.Gson;
import com.iralauf.investment.coravita.backgroundtask.workers.GetWorldInDataWorker;
import com.iralauf.investment.coravita.backgroundtask.workers.InsertDataWorker;
import com.iralauf.investment.coravita.backgroundtask.workers.RiskCalculationWorker;
import com.iralauf.investment.coravita.models.ScanBluetooth;
import com.iralauf.investment.coravita.persistence.AppDatabaseClient;
import com.iralauf.investment.coravita.persistence.WorldData.WorldData;
import com.iralauf.investment.coravita.utils.SharedPrefUtils;
import com.iralauf.investment.coravita.views.widget.DataUpdateWidgetWorker;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import co.lujun.lmbluetoothsdk.BluetoothController;
import co.lujun.lmbluetoothsdk.BluetoothLEController;
import co.lujun.lmbluetoothsdk.base.BluetoothLEListener;
import co.lujun.lmbluetoothsdk.base.BluetoothListener;

import static com.iralauf.investment.coravita.utils.Common.DistanceInMeter;
import static com.iralauf.investment.coravita.utils.Common.KEY_COUNTERTAG;
import static com.iralauf.investment.coravita.utils.Common.KEY_LATITUDE;
import static com.iralauf.investment.coravita.utils.Common.KEY_LONGITUDE;
import static com.iralauf.investment.coravita.utils.Common.KEY_MINIMUMDEVICE_COUNT;
import static com.iralauf.investment.coravita.utils.Common.KEY_SCANNED_DATA;
import static com.iralauf.investment.coravita.utils.Common.errorLogger;
import static com.iralauf.investment.coravita.utils.Common.getDate;
import static com.iralauf.investment.coravita.utils.Common.getDistance;

public class ActiveModeScanningService extends Service {
    /* ********************************************************************************************** */

    String TAG = ActiveModeScanningService.class.getSimpleName();
    private ArrayList<ScanBluetooth> scannedDevicesList = new ArrayList<>();
    public BluetoothLEController mModernBLEController = null;
    public BluetoothController mClassicBTController = null;
    private int COUNT_TAG=1;
    SharedPrefUtils sharedPrefUtils;
    int numberOfIteration=14;
    public static int uniqueDeviceCount=3;
    int stopScanningAfter=10000;//ms=10seconds
    /* ********************************************************************************************** */

    public ActiveModeScanningService() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
    /* ********************************************************************************************** */

    @Override
    public void onCreate() {
        try {
            sharedPrefUtils=new SharedPrefUtils(getApplicationContext());
            mClassicBTController = BluetoothController.getInstance().build(getApplicationContext());
            mModernBLEController = BluetoothLEController.getInstance().build(getApplicationContext());
            new Thread(() -> {
                WorldData worldData=AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().worldDataDao().getCurrentWorldData(getDate());
                if(worldData==null){
                    getWorldInData();
                }
            }).start();
            scheduleWidgetUpdater();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /* ********************************************************************************************** */

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        double latitude = 0;
        double longitude = 0;
        if(COUNT_TAG<=numberOfIteration && sharedPrefUtils.getScanningMode()){
            try {
                latitude = sharedPrefUtils.getCurrentLatitude();
                longitude = sharedPrefUtils.getCurrentLongitude();
                Log.e(TAG, "onStartCommand: Latitude - "+ latitude);
                Log.e(TAG, "onStartCommand: Longitude - "+ longitude);
            } catch (Exception e) {
                e.printStackTrace();
            }
            scannedDevicesList = new ArrayList<>();
            startBluetoothScan(latitude, longitude, COUNT_TAG);
            Log.e(TAG, "onStartCommand: COUNT_TAG - "+ COUNT_TAG++);
        }else{
            if(COUNT_TAG==numberOfIteration+1){
                Log.e(TAG, "onStartCommand: Cancel the alarm and restart - its 14 round");//in future it would be 5 minutes
                Log.e(TAG, "onStartCommand: Calculate the total device count after 28 minutes");//in future it would be 1 hour
                runRiskCalculator();
                COUNT_TAG=1;
                Log.e(TAG, "onStartCommand: Current countTag"+ COUNT_TAG);
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }
    /* ********************************************************************************************** */

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void startBluetoothScan(double latitude, double longitude, int ct) {
        BluetoothAdapter bAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bAdapter.isEnabled()) {
            bAdapter.enable();
        }
        scanClassicBluetooth();
        scanModernBLEBluetooth();
        triggerStopScanning(latitude, longitude, ct);

    }
    /* ********************************************************************************************** */

    public void triggerStopScanning(double latitude, double longitude, int ct) {
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(() -> {
            stopBluetoothScanning(latitude, longitude, ct);
        }, stopScanningAfter);

    }
    /* ********************************************************************************************** */

    public static void getWorldInData() {
        Constraints myConstraints = new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build();
        OneTimeWorkRequest runWorldDataRetrievalWorker = new OneTimeWorkRequest.Builder(GetWorldInDataWorker.class).setConstraints(myConstraints).build();
        WorkManager.getInstance().enqueue(runWorldDataRetrievalWorker);
    }

    private void runRiskCalculator(){
        Data data=new Data.Builder().putInt(KEY_MINIMUMDEVICE_COUNT, uniqueDeviceCount).build();
        OneTimeWorkRequest runRiskCalculationEveryHour = new OneTimeWorkRequest.Builder(RiskCalculationWorker.class).setInputData(data).build();
        WorkManager.getInstance().enqueue(runRiskCalculationEveryHour);
    }
    /* ********************************************************************************************** */

    private void insertScanDataWorker(double latitude, double longitude, int countTag) {
        String jsonText =  new Gson().toJson(scannedDevicesList);
        Data data=new Data.Builder()
                .putDouble(KEY_LATITUDE, latitude)
                .putDouble(KEY_LONGITUDE, longitude)
                .putInt(KEY_COUNTERTAG, countTag)
                .putString(KEY_SCANNED_DATA, jsonText)
                .build();
        OneTimeWorkRequest runWorldDataRetrievalWorker = new OneTimeWorkRequest.Builder(InsertDataWorker.class)
                .setInputData(data).build();
        WorkManager.getInstance().enqueue(runWorldDataRetrievalWorker);

    }
    /* ********************************************************************************************** */

    void stopBluetoothScanning(double latitude, double longitude, int ct) {
        try {
            if (mModernBLEController != null && mClassicBTController != null) {
                if (mModernBLEController.isAvailable() && mClassicBTController.isAvailable()) {
                    mClassicBTController.cancelScan();
                    mModernBLEController.cancelScan();
                    errorLogger(null, "stopBluetoothScanning", TAG);
                    insertScanDataWorker(latitude, longitude, ct);
                }
            }
        } catch (Exception e) {
            errorLogger(e, "stopBluetoothScanning", TAG);
        }
    }
    /* ********************************************************************************************** */
    private void scheduleWidgetUpdater() {
        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .setRequiresBatteryNotLow(true)
                .build();

        PeriodicWorkRequest periodicWorkRequest =
                new PeriodicWorkRequest.Builder(DataUpdateWidgetWorker.class, 15, TimeUnit.MINUTES)
                        .setConstraints(constraints)
                        .build();

        WorkManager.getInstance().enqueue(periodicWorkRequest);
    }
    //**********************************************************************************************
    public void addScannedDeviceToList(BluetoothDevice bluetoothDevice, short rssi) {
        int deviceClass = bluetoothDevice.getBluetoothClass().getMajorDeviceClass();
        if (deviceClass == BluetoothClass.Device.Major.PHONE ||
                deviceClass == BluetoothClass.Device.Major.AUDIO_VIDEO ||
                deviceClass == BluetoothClass.Device.Major.WEARABLE ||
                deviceClass == BluetoothClass.Device.Major.UNCATEGORIZED ||
                deviceClass == BluetoothClass.Device.Major.MISC) {
            scannedDevicesList.add(new ScanBluetooth(bluetoothDevice, String.valueOf(rssi)));
        }
    }
    /* ********************************************************************************************** */

    public void scanModernBLEBluetooth() {

        if (mModernBLEController.isAvailable() && mModernBLEController.isEnabled() && mModernBLEController.isSupportBLE()) {

            mModernBLEController.startScan();
            mModernBLEController.setBluetoothListener(new BluetoothLEListener() {
                @Override
                public void onReadData(BluetoothGattCharacteristic characteristic) {

                }

                @Override
                public void onWriteData(BluetoothGattCharacteristic characteristic) {

                }

                @Override
                public void onDataChanged(BluetoothGattCharacteristic characteristic) {

                }

                @Override
                public void onDiscoveringCharacteristics(List<BluetoothGattCharacteristic> characteristics) {

                }

                @Override
                public void onDiscoveringServices(List<BluetoothGattService> services) {

                }

                @Override
                public void onActionStateChanged(int preState, int state) {

                }

                @Override
                public void onActionDiscoveryStateChanged(String discoveryState) {

                }

                @Override
                public void onActionScanModeChanged(int preScanMode, int scanMode) {

                }

                @Override
                public void onBluetoothServiceStateChanged(int state) {
                }

                @Override
                public void onActionDeviceFound(BluetoothDevice device, short rssi) {
                    if (device != null) {
                        if (Double.parseDouble(getDistance(rssi)) < DistanceInMeter) {
                            addScannedDeviceToList(device, rssi);
                        }
                    }
                }
            });

        }
    }
    /* ********************************************************************************************** */

    public void scanClassicBluetooth() {

        if (mClassicBTController.isAvailable() && mClassicBTController.isEnabled()) {

            mClassicBTController.startScan();

            mClassicBTController.setBluetoothListener(new BluetoothListener() {
                @Override
                public void onReadData(BluetoothDevice device, byte[] data) {

                }

                @Override
                public void onActionStateChanged(int preState, int state) {
                }

                @Override
                public void onActionDiscoveryStateChanged(String discoveryState) {

                }

                @Override
                public void onActionScanModeChanged(int preScanMode, int scanMode) {

                }

                @Override
                public void onBluetoothServiceStateChanged(int state) {
                }

                @Override
                public void onActionDeviceFound(BluetoothDevice device, short rssi) {
                    if (device != null) {
                        if (Double.parseDouble(getDistance(rssi)) < DistanceInMeter) {
                            addScannedDeviceToList(device, rssi);
                        }
                    }
                }
            });
        }
    }
    /* ********************************************************************************************** */

}
