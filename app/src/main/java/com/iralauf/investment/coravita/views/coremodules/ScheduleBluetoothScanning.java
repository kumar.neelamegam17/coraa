package com.iralauf.investment.coravita.views.coremodules;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Date;

public class ScheduleBluetoothScanning extends Worker {

    private final String TAG = ScheduleBluetoothScanning.class.getSimpleName();
    Context context;

    public ScheduleBluetoothScanning(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
    }


    @NonNull
    @Override
    public Result doWork() {
        Log.e(TAG, "calls every 15 minutes "+ new Date());
        //get all last 12 scans and calculate the risk value
        //store the risk value in the risk table
        //clean up
        return Result.success();
    }


}
