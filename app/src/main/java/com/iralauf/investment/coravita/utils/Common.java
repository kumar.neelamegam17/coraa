package com.iralauf.investment.coravita.utils;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.BounceInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.iralauf.investment.coravita.R;
import com.iralauf.investment.coravita.models.AlertMode;
import com.iralauf.investment.coravita.models.DeviceType;
import com.iralauf.investment.coravita.models.MajorDeviceClass;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import lucifer.org.snackbartest.Icon;
import lucifer.org.snackbartest.MySnack;

public class Common {
    /* ********************************************************************************************** */
    public static String TAG=Common.class.getSimpleName();
    public static String defaultLanguage = "en";
    public static String germanLanguage = "de";
    public static Integer LOCATION_INTERVAL=300000;//Millisecond-5 Minutes
    public static Integer BLUETOOTH_SCANNING_INTERVAL=10000;
    public static Integer SCHEDULER_INTERVAL_IN_MINUTES=15;
    public static Integer DistanceInMeter=2;
    public static String DATETIME_FORMAT="YYYY-MM-DD HH:MM:SS";

    public static String KEY_SELECTED_LANGUAGE="language";
    public static String KEY_DARKMODE="darkmode";
    public static String KEY_SCANNINGMODE="scanmode";
    public static String KEY_NETWORKSTATUS="networkstatus";
    public static String KEY_LATITUDE="latitude";
    public static String KEY_LONGITUDE="longitude";
    public static String KEY_CURRENTCOUNTRY="country";
    public static String KEY_CURRENTCITY="city";
    public static String KEY_LASTRISKVALUE="riskvalue";
    public static String KEY_COUNTERTAG="countertag";
    public static String KEY_SCANNED_DATA="scanneddata";
    public static String KEY_MINIMUMDEVICE_COUNT="minimumdevicecount";

    public static final String OUR_WORLD_IN_DATA_JSON="/data/owid-covid-data.json";

    public static String SIMPLEDATEFORMAT="yyyy-MM-dd";
    /* ********************************************************************************************** */

    public static Date getCurrentDateTime() {
        return Calendar.getInstance().getTime();
    }

    public static String getDateString(Date dateTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        return sdf.format(dateTime);
    }

    public static String getDate(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat(SIMPLEDATEFORMAT);
        return formatter.format(calendar.getTime());
    }

    public static String get24DateBefore(){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -24);
        Date sevenDaysAgo = cal.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat(SIMPLEDATEFORMAT);
        return formatter.format(sevenDaysAgo.getTime());
    }

    public static String get1DateBefore(){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -1);
        Date sevenDaysAgo = cal.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat(SIMPLEDATEFORMAT);
        return formatter.format(sevenDaysAgo.getTime());
    }

    public static String convertRiskDecimals(double riskValue){
        BigDecimal bd = BigDecimal.valueOf(riskValue);
        return bd.setScale(2, RoundingMode.DOWN).toString();
    }

    public static String convertMillisecondsToMinutes(long ms){
        return String.valueOf(TimeUnit.MILLISECONDS.toMinutes(ms));
    }


    /* ********************************************************************************************** */

    public static String getCurrentTimeStamp(){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return String.valueOf(timestamp.getTime());
    }
    /* ********************************************************************************************** */

    public static void showToast(String message, int duration, Context context) {
        Toast.makeText(context, message, duration).show();
    }
    /* ********************************************************************************************** */

    public static void errorLogger(Exception e, String message, String TAG) {
        Log.e(TAG, "errorLogger-Message: " + message, e);
    }
    /* ********************************************************************************************** */

    public static void showSnackBar(Activity activity, String message){
        new MySnack.SnackBuilder(activity)
                .setText(message)
                .setTextColor("#ffffff")
                .setTextSize(20)
                .setDurationInSeconds(8)
                .setIcon(Icon.SUCCESS)
                .build();
    }
    /* ********************************************************************************************** */


    public static String getDevTypeDescription(int devType) {
        switch (devType) {
            case BluetoothDevice.DEVICE_TYPE_CLASSIC:
                return DeviceType.CLASSIC.name();
            case BluetoothDevice.DEVICE_TYPE_DUAL:
                return DeviceType.DUAL.name();
            case BluetoothDevice.DEVICE_TYPE_LE:
                return DeviceType.LE.name();
            case BluetoothDevice.DEVICE_TYPE_UNKNOWN:
                return DeviceType.UNKNOWN.name();
            default:
                return DeviceType.UNKNOWN.name() + devType;
        }
    }
    /* ********************************************************************************************** */

    public static void doBounceAnimation(View targetView, int repeatCount) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(targetView, "translationY", 0, 100, 0);
        animator.setInterpolator(new BounceInterpolator());
        animator.setStartDelay(500);
        animator.setDuration(2500);
        animator.setRepeatCount(repeatCount);
        animator.start();
    }
    /* ********************************************************************************************** */

    public static void setUpTheme(Context ctx) {
        SharedPrefUtils sharedPrefUtils = SharedPrefUtils.getInstance(ctx);
        if (sharedPrefUtils.isDarkMode()) ctx.setTheme(R.style.AppTheme_Dark);
        else ctx.setTheme(R.style.AppTheme_Light);
    }
    /* ********************************************************************************************** */

    public static String getDeviceMajorClass(int deviceMajorClass){
        switch (deviceMajorClass){
            case BluetoothClass.Device.Major.PHONE:
                return MajorDeviceClass.PHONE.name();
            case BluetoothClass.Device.Major.HEALTH:
                return MajorDeviceClass.HEALTH.name();
            case BluetoothClass.Device.Major.AUDIO_VIDEO:
                return MajorDeviceClass.AUDIO_VIDEO.name();
            case BluetoothClass.Device.Major.MISC:
                return MajorDeviceClass.MISC.name();
            case BluetoothClass.Device.Major.UNCATEGORIZED:
                return MajorDeviceClass.UNCATEGORIZED.name();
            case BluetoothClass.Device.Major.WEARABLE:
                return MajorDeviceClass.WEARABLE.name();
            default:
                return "NA";
        }
    }
    /* ********************************************************************************************** */

    public static String getDistance(int rssi) {
        /*
         * RSSI = TxPower - 10 * n * lg(d)
         * n = 2 (in free space)
         * d = 10 ^ ((TxPower - RSSI) / (10 * n))
         */
        int txPower = -59; //hard coded power value. Usually ranges between -59 to -65
        double result=Math.pow(10d, ((double) txPower - rssi) / (10 * 2));
        DecimalFormat df = new DecimalFormat("0.00");
        return  df.format(result);
    }
    /* ********************************************************************************************** */

    public static void SnackBar(Context ctx, String Message, View parentLayout, int id) {

        Snackbar mSnackBar = Snackbar.make(parentLayout, Message, Snackbar.LENGTH_LONG);
        View view = mSnackBar.getView();
        view.setPadding(5, 5, 5, 5);

        if (id == 1)//Positive
        {
            view.setBackgroundColor(ctx.getResources().getColor(R.color.green_500));
        } else if (id == 2)//Negative
        {
            view.setBackgroundColor(ctx.getResources().getColor(R.color.md_deep_orange_300));
        } else//Negative
        {
            view.setBackgroundColor(ctx.getResources().getColor(R.color.md_deep_orange_300));
        }


        TextView mainTextView =  (view).findViewById(R.id.snackbar_text);
        mainTextView.setAllCaps(true);
        mainTextView.setTextSize(16);
        mainTextView.setTextColor(ctx.getResources().getColor(R.color.md_white_1000));
        mSnackBar.setDuration(3000);
        mSnackBar.show();
    }
    /* ********************************************************************************************** */

    public static void showCustomDialog(Context ctx, String title, String message, AlertMode alertMode) {
        final Dialog dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.commondialog);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        AppCompatButton dButton=dialog.findViewById(R.id.dialog_bt_close);
        LinearLayoutCompat dLayout=dialog.findViewById(R.id.dialog_bg);
        AppCompatTextView dTitle=dialog.findViewById(R.id.dialog_title);
        AppCompatTextView dMessage=dialog.findViewById(R.id.dialog_content);
        AppCompatImageView dIcon=dialog.findViewById(R.id.dialog_icon);

        if(AlertMode.WARNING == alertMode)
        {
            dLayout.setBackground(ContextCompat.getDrawable(ctx, R.color.orange_400));
            dIcon.setBackgroundDrawable(ContextCompat.getDrawable(ctx, R.drawable.vec_warning));
        }else if(AlertMode.SUCCESS == alertMode){
            dLayout.setBackground(ContextCompat.getDrawable(ctx, R.color.green_400));
            dIcon.setBackgroundDrawable(ContextCompat.getDrawable(ctx, R.drawable.vec_success));
        }else if(AlertMode.ERROR == alertMode){
            dLayout.setBackground(ContextCompat.getDrawable(ctx, R.color.red_400));
            dIcon.setBackgroundDrawable(ContextCompat.getDrawable(ctx, R.drawable.vec_error));
        }else if(AlertMode.RETRY == alertMode){
            dLayout.setBackground(ContextCompat.getDrawable(ctx, R.color.yellow_400));
            dIcon.setBackgroundDrawable(ContextCompat.getDrawable(ctx, R.drawable.vec_retry));
        }

        dTitle.setText(title);
        dMessage.setText(message);
        dButton.setOnClickListener(v -> dialog.dismiss());

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
    /* ********************************************************************************************** */
    public static String getCountryCodeFromCountry(String countryName){
        String[] isoCountries = Locale.getISOCountries();
        String countryCode=null;
        for (String country : isoCountries) {
            Locale locale = new Locale("en", country);
            String iso = locale.getISO3Country();
            String code = locale.getCountry();
            String name = locale.getDisplayCountry();
            if(countryName.equals(name))
            {
                Log.e("CodeFromCountry", iso + " " + code + " " + name);
                countryCode=iso;
            }
        }
        return countryCode;
    }
    /* ********************************************************************************************** */


}
