package com.iralauf.investment.coravita.persistence;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.iralauf.investment.coravita.persistence.RiskData.RiskDao;
import com.iralauf.investment.coravita.persistence.RiskData.RiskObject;
import com.iralauf.investment.coravita.persistence.ScanData.ScanDao;
import com.iralauf.investment.coravita.persistence.ScanData.ScanObject;
import com.iralauf.investment.coravita.persistence.ScanResults.ScanResultsDao;
import com.iralauf.investment.coravita.persistence.ScanResults.ScanResultsObject;
import com.iralauf.investment.coravita.persistence.SettingsData.AppSettingsDao;
import com.iralauf.investment.coravita.persistence.SettingsData.AppSettingsObject;
import com.iralauf.investment.coravita.persistence.WorldData.WorldData;
import com.iralauf.investment.coravita.persistence.WorldData.WorldDataDao;

@Database(entities = {ScanObject.class, ScanResultsObject.class, RiskObject.class, AppSettingsObject.class, WorldData.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ScanDao scanDao();
    public abstract ScanResultsDao scanResultsDao();
    public abstract RiskDao riskDao();
    public abstract WorldDataDao worldDataDao();
    public abstract AppSettingsDao appSettingsDao();
}

