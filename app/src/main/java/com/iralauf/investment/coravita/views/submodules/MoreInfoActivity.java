package com.iralauf.investment.coravita.views.submodules;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import com.iralauf.investment.coravita.R;
import com.iralauf.investment.coravita.databinding.ActivityMoreinfoBinding;
import com.iralauf.investment.coravita.utils.SharedPrefUtils;

import static com.iralauf.investment.coravita.utils.Common.errorLogger;

public class MoreInfoActivity extends AppCompatActivity {

    public static final String TAG = MoreInfoActivity.class.getSimpleName();
    ActivityMoreinfoBinding activityMoreinfoBinding;

    //**********************************************************************************************
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            activityMoreinfoBinding = ActivityMoreinfoBinding.inflate(getLayoutInflater());
            View view = activityMoreinfoBinding.getRoot();
            setContentView(view);
            setUpActionBar();
            init();

        } catch (Exception e) {
            errorLogger(e, "onCreate: ", TAG);
        }
    }

    private void init() {
        Spanned howappworks = Html.fromHtml( getString(R.string.how_this_app_works_for_you_desc));
        Spanned howtomanagecovid_desc = Html.fromHtml( getString(R.string.howtomanagecovid_desc));
        Spanned appcustomize_desc = Html.fromHtml( getString(R.string.appcustomize_desc));
        activityMoreinfoBinding.howthisappworks.setText(howappworks);
        activityMoreinfoBinding.howtomanagecovid.setText(howtomanagecovid_desc);
        activityMoreinfoBinding.appcustomize.setText(appcustomize_desc);
    }
    //**********************************************************************************************

    private void setUpActionBar() {
        setUpTheme();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.moreinfo_activity_title));
    }
    //**********************************************************************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    //**********************************************************************************************
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    //**********************************************************************************************
    public void setUpTheme() {
        SharedPrefUtils sharedPrefUtils = SharedPrefUtils.getInstance(this);
        if (sharedPrefUtils.isDarkMode()) {
            setTheme(R.style.AppTheme_Dark);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorthemeDarkPrimary)));
        } else {
            setTheme(R.style.AppTheme_Light);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorthemeLightPrimary)));
        }
    }
    //**********************************************************************************************

}
