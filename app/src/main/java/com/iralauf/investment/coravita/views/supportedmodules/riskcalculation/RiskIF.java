package com.iralauf.investment.coravita.views.supportedmodules.riskcalculation;

import android.content.Context;

public interface RiskIF {
    void RiskCalculation(Context context);
    String CalculateRisk(double prevalenceP, int noOfDevicesN);
    void PersistRiskHourly(String riskValue);
    void PersistRiskPerDay(String riskValue);
}
