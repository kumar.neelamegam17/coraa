package com.iralauf.investment.coravita.views.supportedmodules.riskcalculation;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;

public class Utils {

    public static final BigInteger nchoosek(int n, int k) {
        BigInteger result = BigInteger.ONE;
        int i = 0;
        for (int var5 = k; i < var5; ++i) {
            result = result.multiply(BigInteger.valueOf((long) n - (long) i)).divide(BigInteger.valueOf((long) i + 1L));
        }
        return result;
    }

    public static final BigDecimal equal(double p, int n, int x) {
        BigDecimal bin = new BigDecimal(nchoosek(n, x));
        double var8 = x;
        BigDecimal var10000 = bin.multiply(BigDecimal.valueOf(Math.pow(p, var8)));
        double var6 = (double) 1 - p;
        var8 = (double) n - (double) x;
        var10000 = var10000.multiply(BigDecimal.valueOf(Math.pow(var6, var8)));
        return var10000;
    }

    public static final BigDecimal less(double p, int n, int x) {
        BigDecimal result = BigDecimal.ZERO;
        int i = 0;
        for (int var7 = x; i < var7; ++i) {
            result = result.add(equal(p, n, i));
        }
        return result;
    }

    public static final BigDecimal greater(double p, int n, int x) {
        BigDecimal result = BigDecimal.ZERO;
        int i = n;
        int var7 = x + 1;
        if (n >= var7) {
            while (true) {
                result = result.add(equal(p, n, i));
                if (i == var7) {
                    break;
                }
                --i;
            }
        }
        return result;
    }

    public static final double mean(double p, int n, int x) {
        return (double) n * p;
    }

    public static final double variance(double p, int n, int x) {
        return (double) n * p * ((double) 1 - p);
    }

    public static final double stddev(double p, int n, int x) {
        double var5 = variance(p, n, x);
        return Math.sqrt(var5);
    }

    public static final String getPrettyNum(Object num) {
        String var10000 = (new DecimalFormat("#0.##########")).format(num);
        return var10000;
    }

    public static final double round(double num, int digits) {
        double var4 = 10.0D;
        double var6 = digits;
        double var10000 = (double) Math.round(num * Math.pow(var4, var6));
        var4 = 10.0D;
        var6 = digits;
        return var10000 / Math.pow(var4, var6);
    }
}
