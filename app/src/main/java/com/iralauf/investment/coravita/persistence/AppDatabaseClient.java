package com.iralauf.investment.coravita.persistence;

import android.content.Context;

import androidx.room.Room;

public class AppDatabaseClient {
    private final Context mCtx;
    private static AppDatabaseClient mInstance;

    //our app database object
    private final AppDatabase appDatabase;

    private AppDatabaseClient(Context mCtx) {
        this.mCtx = mCtx;

        //creating the app database with Room database builder
        //MyToDos is the name of the database
        appDatabase = Room.databaseBuilder(mCtx, AppDatabase.class, "CORAVITA").build();
    }

    public static synchronized AppDatabaseClient getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new AppDatabaseClient(mCtx);
        }
        return mInstance;
    }

    public AppDatabase getAppDatabase() {
        return appDatabase;
    }
}
