package com.iralauf.investment.coravita.models;

public enum DeviceType {
    CLASSIC, LE, DUAL, UNKNOWN
}
