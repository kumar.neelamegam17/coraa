package com.iralauf.investment.coravita.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkRequest;
import android.util.Log;

import androidx.annotation.NonNull;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class CheckNetwork {

    String TAG = CheckNetwork.class.getSimpleName();

    Context context;
    SharedPrefUtils sharedPrefUtils;

    public CheckNetwork(Context context) {
        this.context = context;
        this.sharedPrefUtils=new SharedPrefUtils(context);
    }

    // Network Check
    public void registerNetworkCallback() {
        try {

            final ConnectivityManager manager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
            manager.registerNetworkCallback(
                    new NetworkRequest.Builder()
                            .build(),
                    new ConnectivityManager.NetworkCallback() {
                        @Override
                        public void onAvailable(Network network) {
                            Log.e(TAG, "Network onAvailable");
                            sharedPrefUtils.setNetworkAvailability(true);
                        }

                        @Override
                        public void onUnavailable() {
                            sharedPrefUtils.setNetworkAvailability(false);
                            Log.e(TAG, "Network onUnavailable");
                        }

                        @Override
                        public void onLost(@NonNull Network network) {
                            sharedPrefUtils.setNetworkAvailability(false);
                            Log.e(TAG, "Network onLost");
                        }
                    });

        } catch (Exception e) {
            Common.errorLogger(e, "registerNetworkCallback", TAG);
        }
    }
}
