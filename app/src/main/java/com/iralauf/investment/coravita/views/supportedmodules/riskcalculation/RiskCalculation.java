package com.iralauf.investment.coravita.views.supportedmodules.riskcalculation;

import android.content.Context;

import com.iralauf.investment.coravita.persistence.AppDatabaseClient;
import com.iralauf.investment.coravita.persistence.RiskData.RiskObject;

import java.math.BigDecimal;

import static com.iralauf.investment.coravita.utils.Common.getCurrentDateTime;

public class RiskCalculation implements RiskIF {

    Context context;

    @Override
    public void RiskCalculation(Context context) {
        this.context = context;
    }

    @Override
    public String CalculateRisk(double prevalenceP, int noOfDevicesN) {

        int X=1;

        BigDecimal equalResult = Utils.equal(prevalenceP, noOfDevicesN, X);
        BigDecimal lessResult = Utils.less(prevalenceP, noOfDevicesN, X);
        BigDecimal greaterResult = Utils.greater(prevalenceP, noOfDevicesN, X);
        BigDecimal lessEqualResult = equalResult.add(lessResult);
        BigDecimal greaterEqualResult = equalResult.add(greaterResult);

        //BigInteger nchoosek = Utils.nchoosek(noOfDevicesN, X);
        //double mean = Utils.round(Utils.mean(prevalenceP, noOfDevicesN, X), 5);
        //double variance = Utils.round(Utils.variance(prevalenceP, noOfDevicesN, X), 5);
        //double stddev = Utils.round(Utils.stddev(prevalenceP, noOfDevicesN, X), 5);

        //System.out.println(Utils.getPrettyNum(equalResult));
        //System.out.println(Utils.getPrettyNum(lessResult));
        //System.out.println(Utils.getPrettyNum(greaterResult));
        //System.out.println(Utils.getPrettyNum(lessEqualResult));
        System.out.println("Calculated BDC = " + Utils.getPrettyNum(greaterEqualResult));
        PersistRiskHourly(Utils.getPrettyNum(greaterEqualResult));
        //System.out.println(Utils.getPrettyNum(nchoosek));
        //System.out.println(Utils.getPrettyNum(mean));
        //System.out.println(Utils.getPrettyNum(variance));
        //System.out.println(Utils.getPrettyNum(stddev));
        return Utils.getPrettyNum(greaterEqualResult);
    }

    @Override
    public void PersistRiskHourly(String riskValue) {
        new Thread(() -> {
            RiskObject riskObject=new RiskObject();
            riskObject.setActive(true);
            riskObject.setCreatedAt(getCurrentDateTime());
            riskObject.setRiskValue(riskValue);
            AppDatabaseClient.getInstance(context).getAppDatabase().riskDao().insert(riskObject);
        }).start();
    }

    @Override
    public void PersistRiskPerDay(String riskValue) {

    }
}
