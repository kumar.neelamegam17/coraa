package com.iralauf.investment.coravita.views.submodules;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import com.iralauf.investment.coravita.R;
import com.iralauf.investment.coravita.databinding.ActivityStatisticsBinding;
import com.iralauf.investment.coravita.utils.SharedPrefUtils;

import static com.iralauf.investment.coravita.utils.Common.errorLogger;

public class StatisticsActivity extends AppCompatActivity {

    public static final String TAG = StatisticsActivity.class.getSimpleName();
    ActivityStatisticsBinding activityStatisticsBinding;

    //**********************************************************************************************
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            activityStatisticsBinding = ActivityStatisticsBinding.inflate(getLayoutInflater());
            View view = activityStatisticsBinding.getRoot();
            setContentView(view);
            setUpActionBar();

        } catch (Exception e) {
            errorLogger(e, "onCreate: ", TAG);
        }
    }
    //**********************************************************************************************

    private void setUpActionBar() {
        setUpTheme();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.statistics_activity_title));
    }
    //**********************************************************************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    //**********************************************************************************************
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    //**********************************************************************************************
    public void setUpTheme() {
        SharedPrefUtils sharedPrefUtils = SharedPrefUtils.getInstance(this);
        if (sharedPrefUtils.isDarkMode()) {
            setTheme(R.style.AppTheme_Dark);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorthemeDarkPrimary)));
        } else {
            setTheme(R.style.AppTheme_Light);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorthemeLightPrimary)));
        }
    }
}
