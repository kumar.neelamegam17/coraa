package com.iralauf.investment.coravita.views.coremodules;


import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.Wave;
import com.iralauf.investment.coravita.R;
import com.iralauf.investment.coravita.databinding.ActivitySplashBinding;
import com.iralauf.investment.coravita.utils.CheckNetwork;
import com.iralauf.investment.coravita.utils.SharedPrefUtils;
import com.yariksoffice.lingver.Lingver;

import static com.iralauf.investment.coravita.utils.Common.doBounceAnimation;

public class SplashActivity extends CoreActivity {

    private static final long SPLASH_DURATION = 3000;
    SpinKitView spinKitView;
    SharedPrefUtils sharedPrefUtils;
    ActivitySplashBinding activitySplashBinding;

    //**********************************************************************************************
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefUtils sharedPrefUtils = SharedPrefUtils.getInstance(this);
        if (sharedPrefUtils.isDarkMode()) setTheme(R.style.AppTheme_Dark);
        else setTheme(R.style.AppTheme_Light);
        activitySplashBinding= ActivitySplashBinding.inflate(getLayoutInflater());
        View view = activitySplashBinding.getRoot();
        setContentView(view);


        try {
            isStoragePermissionGranted();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //**********************************************************************************************
    @Override
    public void onPermissionsGranted(int requestCode) {

        try {

            sharedPrefUtils = SharedPrefUtils.getInstance(this);
            if (!sharedPrefUtils.getLatestLanguage().equals("")) {
                Lingver.getInstance().setLocale(this, sharedPrefUtils.getLatestLanguage());   //set the saved language
                sharedPrefUtils.saveLatestLanguage(sharedPrefUtils.getLatestLanguage());
            }

            getInit();

        } catch (Exception e) {

            e.printStackTrace();
        }


    }

    //**********************************************************************************************
    private void getInit() {

        Sprite animate = new Wave();
        activitySplashBinding.progressBar.setIndeterminateDrawable(animate);
        doBounceAnimation(activitySplashBinding.imageView, 2);
        CheckNetwork checkNetwork=new CheckNetwork(this);
        checkNetwork.registerNetworkCallback();

        final BluetoothAdapter bAdapter = BluetoothAdapter.getDefaultAdapter();
        if(bAdapter == null)
        {
            Toast.makeText(getApplicationContext(),R.string.nobluetoothsupport,Toast.LENGTH_SHORT).show();
        }
        else{
            if(!bAdapter.isEnabled()){
                bAdapter.enable();
            }
            new Handler(Looper.myLooper()).postDelayed(() -> navigate(), SPLASH_DURATION);
        }
    }

    //**********************************************************************************************
    @Override
    protected void onResume() {
        super.onResume();
    }

    private void navigate() {
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
        finishAffinity();
    }

    //**********************************************************************************************
    @Override
    public void onBackPressed() {

    }

    //**********************************************************************************************
    @Override
    protected void bindViews() {

    }

    //**********************************************************************************************
    @Override
    protected void setListeners() {

    }

    //**********************************************************************************************
    @Override
    protected void onPause() {
        super.onPause();
    }

    //**********************************************************************************************
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    //**********************************************************************************************
    @Override
    protected void onStop() {
        super.onStop();
    }

//**********************************************************************************************
}
