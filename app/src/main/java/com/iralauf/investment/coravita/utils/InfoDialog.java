package com.iralauf.investment.coravita.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import androidx.annotation.NonNull;

import com.iralauf.investment.coravita.R;
import com.iralauf.investment.coravita.databinding.DialogInfoBinding;
import com.iralauf.investment.coravita.models.RiskLevels;




public class InfoDialog extends Dialog {

    private final RiskLevels riskLevels;
    private final Context context;
    DialogInfoBinding dialogInfoBinding;

    public InfoDialog(@NonNull Context context, RiskLevels riskLevels) {
        super(context);
        this.context = context;
        this.riskLevels = riskLevels;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogInfoBinding = DialogInfoBinding.inflate(getLayoutInflater());
        View view=dialogInfoBinding.getRoot();
        setContentView(view);
        Window window = getWindow();
        if (window != null) {
            window.getAttributes().windowAnimations = R.style.DialogAnimation;
            window.setBackgroundDrawableResource(android.R.color.transparent);
        }
        setupViews();
    }


    private void setupViews() {
        int color;
        switch (riskLevels) {
            case LOW:
                color = context.getResources().getColor(R.color.scaleLow);
                dialogInfoBinding.riskScale.setText(R.string.low_risk_scale);
                dialogInfoBinding.riskLevel.setText(R.string.low_risk);
                dialogInfoBinding.riskLevel.setBackgroundColor(color);
                dialogInfoBinding.healthImplications.setText(R.string.low_risk_implication);
                dialogInfoBinding.imgvwSymbol.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_smile_lowrisk));
                dialogInfoBinding.parentlayout.setCardBackgroundColor(color);
                break;
            case MODERATE:
                color = context.getResources().getColor(R.color.scaleModerate);
                dialogInfoBinding.riskScale.setText(R.string.moderate_risk_scale);
                dialogInfoBinding.riskLevel.setText(R.string.moderate_risk);
                dialogInfoBinding.riskLevel.setTextColor(context.getResources().getColor(R.color.black));
                dialogInfoBinding.riskLevel.setBackgroundColor(color);
                dialogInfoBinding.healthImplications.setText(R.string.moderate_risk_implication);
                dialogInfoBinding.healthImplications.setTextColor(context.getResources().getColor(R.color.black));
                dialogInfoBinding.imgvwSymbol.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_smile_moderaterisk));
                dialogInfoBinding.parentlayout.setCardBackgroundColor(color);
                break;
            case HIGH:
                color = context.getResources().getColor(R.color.scaleHigh);
                dialogInfoBinding.riskScale.setText(R.string.high_risk_scale);
                dialogInfoBinding.riskLevel.setText(R.string.high_risk);
                dialogInfoBinding.riskLevel.setBackgroundColor(color);
                dialogInfoBinding.healthImplications.setText(R.string.high_risk_implication);
                dialogInfoBinding.imgvwSymbol.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_smile_highrisk));
                dialogInfoBinding.parentlayout.setCardBackgroundColor(color);
                break;
            case VERYHIGH:
                color = context.getResources().getColor(R.color.scaleVeryHigh);
                dialogInfoBinding.riskScale.setText(R.string.veryhigh_risk_scale);
                dialogInfoBinding.riskLevel.setText(R.string.veryhigh_risk);
                dialogInfoBinding.riskLevel.setBackgroundColor(color);
                dialogInfoBinding.healthImplications.setText(R.string.veryhigh_risk_implication);
                dialogInfoBinding.imgvwSymbol.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_smile_veryhighrisk));
                dialogInfoBinding.parentlayout.setCardBackgroundColor(color);
                break;

        }
    }
}
