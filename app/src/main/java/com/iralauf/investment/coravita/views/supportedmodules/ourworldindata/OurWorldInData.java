package com.iralauf.investment.coravita.views.supportedmodules.ourworldindata;

import com.fasterxml.jackson.annotation.*;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "continent",
        "location",
        "population",
        "population_density",
        "median_age",
        "aged_65_older",
        "aged_70_older",
        "gdp_per_capita",
        "extreme_poverty",
        "cardiovasc_death_rate",
        "diabetes_prevalence",
        "female_smokers",
        "male_smokers",
        "hospital_beds_per_thousand",
        "life_expectancy",
        "human_development_index",
        "data"
})

public class OurWorldInData implements Serializable {

    @JsonProperty("continent")
    private String continent;
    @JsonProperty("location")
    private String location;
    @JsonProperty("population")
    private Integer population;
    @JsonProperty("population_density")
    private Double populationDensity;
    @JsonProperty("median_age")
    private Double medianAge;
    @JsonProperty("aged_65_older")
    private Double aged65Older;
    @JsonProperty("aged_70_older")
    private Double aged70Older;
    @JsonProperty("gdp_per_capita")
    private Double gdpPerCapita;
    @JsonProperty("extreme_poverty")
    private Double extremePoverty;
    @JsonProperty("cardiovasc_death_rate")
    private Double cardiovascDeathRate;
    @JsonProperty("diabetes_prevalence")
    private Double diabetesPrevalence;
    @JsonProperty("female_smokers")
    private Double femaleSmokers;
    @JsonProperty("male_smokers")
    private Double maleSmokers;
    @JsonProperty("hospital_beds_per_thousand")
    private Double hospitalBedsPerThousand;
    @JsonProperty("life_expectancy")
    private Double lifeExpectancy;
    @JsonProperty("human_development_index")
    private Double humanDevelopmentIndex;
    @JsonProperty("data")
    private List<Datum> data = null;
    @JsonIgnore
    private final Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("continent")
    public String getContinent() {
        return continent;
    }

    @JsonProperty("continent")
    public void setContinent(String continent) {
        this.continent = continent;
    }

    @JsonProperty("location")
    public String getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(String location) {
        this.location = location;
    }

    @JsonProperty("population")
    public Integer getPopulation() {
        return population;
    }

    @JsonProperty("population")
    public void setPopulation(Integer population) {
        this.population = population;
    }

    @JsonProperty("population_density")
    public Double getPopulationDensity() {
        return populationDensity;
    }

    @JsonProperty("population_density")
    public void setPopulationDensity(Double populationDensity) {
        this.populationDensity = populationDensity;
    }

    @JsonProperty("median_age")
    public Double getMedianAge() {
        return medianAge;
    }

    @JsonProperty("median_age")
    public void setMedianAge(Double medianAge) {
        this.medianAge = medianAge;
    }

    @JsonProperty("aged_65_older")
    public Double getAged65Older() {
        return aged65Older;
    }

    @JsonProperty("aged_65_older")
    public void setAged65Older(Double aged65Older) {
        this.aged65Older = aged65Older;
    }

    @JsonProperty("aged_70_older")
    public Double getAged70Older() {
        return aged70Older;
    }

    @JsonProperty("aged_70_older")
    public void setAged70Older(Double aged70Older) {
        this.aged70Older = aged70Older;
    }

    @JsonProperty("gdp_per_capita")
    public Double getGdpPerCapita() {
        return gdpPerCapita;
    }

    @JsonProperty("gdp_per_capita")
    public void setGdpPerCapita(Double gdpPerCapita) {
        this.gdpPerCapita = gdpPerCapita;
    }

    @JsonProperty("extreme_poverty")
    public Double getExtremePoverty() {
        return extremePoverty;
    }

    @JsonProperty("extreme_poverty")
    public void setExtremePoverty(Double extremePoverty) {
        this.extremePoverty = extremePoverty;
    }

    @JsonProperty("cardiovasc_death_rate")
    public Double getCardiovascDeathRate() {
        return cardiovascDeathRate;
    }

    @JsonProperty("cardiovasc_death_rate")
    public void setCardiovascDeathRate(Double cardiovascDeathRate) {
        this.cardiovascDeathRate = cardiovascDeathRate;
    }

    @JsonProperty("diabetes_prevalence")
    public Double getDiabetesPrevalence() {
        return diabetesPrevalence;
    }

    @JsonProperty("diabetes_prevalence")
    public void setDiabetesPrevalence(Double diabetesPrevalence) {
        this.diabetesPrevalence = diabetesPrevalence;
    }

    @JsonProperty("female_smokers")
    public Double getFemaleSmokers() {
        return femaleSmokers;
    }

    @JsonProperty("female_smokers")
    public void setFemaleSmokers(Double femaleSmokers) {
        this.femaleSmokers = femaleSmokers;
    }

    @JsonProperty("male_smokers")
    public Double getMaleSmokers() {
        return maleSmokers;
    }

    @JsonProperty("male_smokers")
    public void setMaleSmokers(Double maleSmokers) {
        this.maleSmokers = maleSmokers;
    }

    @JsonProperty("hospital_beds_per_thousand")
    public Double getHospitalBedsPerThousand() {
        return hospitalBedsPerThousand;
    }

    @JsonProperty("hospital_beds_per_thousand")
    public void setHospitalBedsPerThousand(Double hospitalBedsPerThousand) {
        this.hospitalBedsPerThousand = hospitalBedsPerThousand;
    }

    @JsonProperty("life_expectancy")
    public Double getLifeExpectancy() {
        return lifeExpectancy;
    }

    @JsonProperty("life_expectancy")
    public void setLifeExpectancy(Double lifeExpectancy) {
        this.lifeExpectancy = lifeExpectancy;
    }

    @JsonProperty("human_development_index")
    public Double getHumanDevelopmentIndex() {
        return humanDevelopmentIndex;
    }

    @JsonProperty("human_development_index")
    public void setHumanDevelopmentIndex(Double humanDevelopmentIndex) {
        this.humanDevelopmentIndex = humanDevelopmentIndex;
    }

    @JsonProperty("data")
    public List<Datum> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(List<Datum> data) {
        this.data = data;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
