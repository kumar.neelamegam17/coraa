package com.iralauf.investment.coravita.views.supportedmodules.prevalencecalculation;

import android.content.Context;

import com.iralauf.investment.coravita.views.supportedmodules.ourworldindata.OurWorldInData;

public interface PrevalenceIF {
    void Prevalence(Context context);
    void CalculatePrevalence(OurWorldInData ourWorldInData);
    void PersistPrevalence(double prevalence);
}
