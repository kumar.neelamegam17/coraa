package com.iralauf.investment.coravita.models;

import android.bluetooth.BluetoothDevice;

public class ScanBluetooth {
    BluetoothDevice bluetoothDevice;
    String rssi;

    public ScanBluetooth(BluetoothDevice bluetoothDevice, String rssi) {
        this.bluetoothDevice = bluetoothDevice;
        this.rssi = rssi;
    }

    public BluetoothDevice getBluetoothDevice() {
        return bluetoothDevice;
    }

    public void setBluetoothDevice(BluetoothDevice bluetoothDevice) {
        this.bluetoothDevice = bluetoothDevice;
    }

    public String getRssi() {
        return rssi;
    }

    public void setRssi(String rssi) {
        this.rssi = rssi;
    }
}
