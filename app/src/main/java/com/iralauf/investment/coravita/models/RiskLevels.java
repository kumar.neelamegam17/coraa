package com.iralauf.investment.coravita.models;

public enum RiskLevels {
    LOW, MODERATE, HIGH, VERYHIGH
}
