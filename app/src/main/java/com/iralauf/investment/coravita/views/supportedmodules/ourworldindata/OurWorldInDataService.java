package com.iralauf.investment.coravita.views.supportedmodules.ourworldindata;

import com.google.gson.JsonObject;
import retrofit2.Call;
import retrofit2.http.GET;

import static com.iralauf.investment.coravita.utils.Common.OUR_WORLD_IN_DATA_JSON;

public interface OurWorldInDataService {
    @GET(OUR_WORLD_IN_DATA_JSON)
    Call<JsonObject> getWorldInData();

}
