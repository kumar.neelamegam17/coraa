package com.iralauf.investment.coravita.persistence.WorldData;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.iralauf.investment.coravita.persistence.DateConverter;

import java.io.Serializable;
import java.util.Date;

@Entity
public class WorldData implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "isactive")
    private boolean isActive;

    @ColumnInfo(name = "created_at")
    @TypeConverters({DateConverter.class})
    private Date createdAt;

    private String worldDataJson;
    private String countryCode;
    private String countryName;
    private String calculatedPrevalence;
    private String lastRetrievedDate;

    public String getLastRetrievedDate() {
        return lastRetrievedDate;
    }

    public void setLastRetrievedDate(String lastRetrievedDate) {
        this.lastRetrievedDate = lastRetrievedDate;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getWorldDataJson() {
        return worldDataJson;
    }

    public void setWorldDataJson(String worldDataJson) {
        this.worldDataJson = worldDataJson;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCalculatedPrevalence() {
        return calculatedPrevalence;
    }

    public void setCalculatedPrevalence(String calculatedPrevalence) {
        this.calculatedPrevalence = calculatedPrevalence;
    }
}
