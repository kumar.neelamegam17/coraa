package com.iralauf.investment.coravita.persistence.WorldData;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface WorldDataDao {
    @Query("SELECT * FROM WorldData")
    List<WorldData> getAll();

    @Insert
    void insert(WorldData task);

    @Delete
    void delete(WorldData task);

    @Update
    void update(WorldData task);


    @Query("SELECT * FROM WorldData WHERE lastRetrievedDate=:currentDate")
    WorldData getCurrentWorldData(String currentDate);

    @Query("DELETE FROM WorldData")
    void deleteAll();

    @Query("UPDATE WorldData SET calculatedPrevalence=:prevalenceResult WHERE isactive=1")
    void updatePrevalenceResult(double prevalenceResult);

}

