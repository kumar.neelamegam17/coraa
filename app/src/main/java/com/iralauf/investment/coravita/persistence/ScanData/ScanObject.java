package com.iralauf.investment.coravita.persistence.ScanData;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.iralauf.investment.coravita.persistence.DateConverter;

import java.io.Serializable;
import java.util.Date;

@Entity
public class ScanObject implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "mac_address")
    private String macAddress;

    @ColumnInfo(name = "device_type")
    private String deviceType;

    @ColumnInfo(name = "bluetooth_type")
    private String bluetooth_type;

    @ColumnInfo(name = "signal_strength")
    private String signalStrength;

    @ColumnInfo(name = "scan_status")
    private String scanStatus;

    @ColumnInfo(name = "isactive")
    private boolean isActive;

    @ColumnInfo(name = "latitude")
    private double latitude;

    @ColumnInfo(name = "longitude")
    private double longitude;

    @ColumnInfo(name = "rssi")
    private String rssi;

    @ColumnInfo(name = "created_at")
    @TypeConverters(DateConverter.class)
    private Date createdAt;

    @ColumnInfo(name = "distance")
    private String distance;

    @ColumnInfo(name = "UUID")
    private String uuid;

    private int countTag;

    public int getCountTag() {
        return countTag;
    }

    public void setCountTag(int countTag) {
        this.countTag = countTag;
    }

    public String getBluetooth_type() {
        return bluetooth_type;
    }

    public void setBluetooth_type(String bluetooth_type) {
        this.bluetooth_type = bluetooth_type;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getRssi() {
        return rssi;
    }

    public void setRssi(String rssi) {
        this.rssi = rssi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getSignalStrength() {
        return signalStrength;
    }

    public void setSignalStrength(String signalStrength) {
        this.signalStrength = signalStrength;
    }

    public String getScanStatus() {
        return scanStatus;
    }

    public void setScanStatus(String scanStatus) {
        this.scanStatus = scanStatus;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
