package com.iralauf.investment.coravita.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;
import static com.iralauf.investment.coravita.utils.Common.*;


public class SharedPrefUtils {
    private static final String SHARED_PREF_NAME = "coravitashared";
    private static SharedPrefUtils INSTANCE = null;
    private final SharedPreferences preferences;

    public static SharedPrefUtils getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new SharedPrefUtils(context);
            return INSTANCE;
        }
        return INSTANCE;
    }

    public SharedPrefUtils(Context context) {
        preferences = context.getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
    }


    public void saveLatestLanguage(String lang) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_SELECTED_LANGUAGE, lang);
        editor.apply();
    }

    public String getLatestLanguage() {
        return preferences.getString(KEY_SELECTED_LANGUAGE, "");
    }


    public void isDarkMode(Boolean b) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_DARKMODE, b);
        editor.apply();
    }

    public Boolean isDarkMode() {
        return preferences.getBoolean(KEY_DARKMODE, false);
    }

    public void setScanningMode(boolean scanMode) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_SCANNINGMODE, scanMode);
        editor.apply();
    }

    public boolean getScanningMode() {
        return preferences.getBoolean(KEY_SCANNINGMODE, true);
    }

    public void clearAllPrefs() {
        preferences.edit().clear().apply();
    }

    public void setCurrentLatitude(double latitude) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(KEY_LATITUDE, Double.doubleToRawLongBits(latitude));
        editor.apply();
    }

    public double getCurrentLatitude() {
        return Double.doubleToRawLongBits(preferences.getLong(KEY_LATITUDE, 0));
    }

    public void setCurrentLongitude(double latitude) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(KEY_LONGITUDE, Double.doubleToRawLongBits(latitude));
        editor.apply();
    }

    public double getCurrentLongitude() {
        return Double.doubleToRawLongBits(preferences.getLong(KEY_LONGITUDE, 0));
    }

    public void setNetworkAvailability(boolean status) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_NETWORKSTATUS, status);
        editor.apply();
    }

    public boolean getNetworkAvailability() {
        return preferences.getBoolean(KEY_NETWORKSTATUS, false);
    }

    public void setCurrentCountry(String country) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_CURRENTCOUNTRY, country);
        editor.apply();
    }

    public String getCurrentCountry() {
        return preferences.getString(KEY_CURRENTCOUNTRY, "");
    }

    public void setCurrentCity(String city) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_CURRENTCITY, city);
        editor.apply();
    }

    public String getCurrentCity() {
        return preferences.getString(KEY_CURRENTCITY, "");
    }

    public void setLastRiskValue(String riskValue) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_LASTRISKVALUE, riskValue);
        editor.apply();
    }

    public String getLastRiskValue() {
        return preferences.getString(KEY_LASTRISKVALUE, "0");
    }


}
