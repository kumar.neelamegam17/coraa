package com.iralauf.investment.coravita.views.supportedmodules.prevalencecalculation;

import android.content.Context;

import com.iralauf.investment.coravita.persistence.AppDatabaseClient;
import com.iralauf.investment.coravita.utils.Common;
import com.iralauf.investment.coravita.views.supportedmodules.ourworldindata.Datum;
import com.iralauf.investment.coravita.views.supportedmodules.ourworldindata.OurWorldInData;

import static com.iralauf.investment.coravita.utils.Common.errorLogger;

public class PrevalenceCalculation implements PrevalenceIF {

    String TAG = PrevalenceCalculation.class.getSimpleName();
    Context context;
    private static final double PRE_DETECTED_DAYS = 7.5;
    private static final double ACTIVE_BLUETOOTH_RATIO = 40;

    @Override
    public void Prevalence(Context context) {
        this.context = context;
    }

    @Override
    public void CalculatePrevalence(OurWorldInData ourWorldInData) {

        try {
            Datum todayDatum = null;
            for (Datum datum : ourWorldInData.getData()) {
                if (datum.getDate().equalsIgnoreCase(Common.getDate())) {
                    todayDatum = datum;
                    break;
                } else if (datum.getDate().equalsIgnoreCase(Common.get1DateBefore())) {
                    todayDatum = datum;
                    break;
                }
            }

            Datum before24Days = null;
            for (Datum datum : ourWorldInData.getData()) {
                if (datum.getDate().equalsIgnoreCase(Common.get24DateBefore())) {
                    before24Days = datum;
                    break;
                }
            }

            //Prevalence Inputs
            double total_number_of_COVID_deaths = todayDatum.getTotalDeaths();
            double total_cumulative_cases = todayDatum.getTotalCases();
            double total_cumulative_cases_as_of_24_days_ago = before24Days.getTotalCases();
            double total_population = ourWorldInData.getPopulation();

            //Peter Sommerer calculation
            double Death_cases_24_days_Ago = total_number_of_COVID_deaths / total_cumulative_cases_as_of_24_days_ago;
            double Increase_of_cases_over_24days = total_cumulative_cases - total_cumulative_cases_as_of_24_days_ago;
            double expected_new_deaths = Math.round(Death_cases_24_days_Ago * Increase_of_cases_over_24days);
            double total_of_deaths = Math.round(total_number_of_COVID_deaths + expected_new_deaths);
            double observed_case_fatility_rate = formatPercentage(total_number_of_COVID_deaths / total_cumulative_cases);
            double extrapolated_total_cases = (total_of_deaths / observed_case_fatility_rate) * 100;
            double percentage_of_cases_detected = Math.round((total_cumulative_cases / extrapolated_total_cases) * 100);

            //Final calculations
            double number_of_new_cases_detected = todayDatum.getNewCases();
            double number_of_new_cases_total = Math.round((number_of_new_cases_detected / percentage_of_cases_detected) * 100);
            double number_of_predetection_days = PRE_DETECTED_DAYS;
            double number_of_predetection_infectious_people = number_of_new_cases_total * number_of_predetection_days;
            double percentage_of_predetection_infectious_people = formatPercentage(number_of_predetection_infectious_people / total_population);
            double bluetooth_active_ratio = ACTIVE_BLUETOOTH_RATIO;
            double prevalence = (percentage_of_predetection_infectious_people / bluetooth_active_ratio) * 100;
            double prevalenceRoundUp = Double.parseDouble(String.format("%.2f", prevalence)) / 100;
            System.out.println("Final Prevalence = " + prevalenceRoundUp);
            PersistPrevalence(prevalenceRoundUp);

        } catch (Exception e) {
            errorLogger(e, "getWorldInData", context.getClass().getSimpleName());
        }

    }

    @Override
    public void PersistPrevalence(double prevalence) {
        new Thread(() -> {
            AppDatabaseClient.getInstance(context).getAppDatabase().worldDataDao().updatePrevalenceResult(prevalence);
        }).start();
    }

    static double formatPercentage(double value) {
        return Double.parseDouble(String.format("%.02f", value * 100));
    }

}
