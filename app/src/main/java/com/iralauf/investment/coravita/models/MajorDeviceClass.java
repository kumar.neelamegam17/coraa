package com.iralauf.investment.coravita.models;

public enum MajorDeviceClass {
    PHONE, UNCATEGORIZED, WEARABLE, MISC, HEALTH, AUDIO_VIDEO
}
