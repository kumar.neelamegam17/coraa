package com.iralauf.investment.coravita.models;

public enum AlertMode {
    WARNING, ERROR, SUCCESS, RETRY
}
