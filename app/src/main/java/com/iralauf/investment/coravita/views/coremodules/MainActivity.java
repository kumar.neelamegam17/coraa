package com.iralauf.investment.coravita.views.coremodules;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.iralauf.investment.coravita.R;
import com.iralauf.investment.coravita.backgroundtask.ActiveModeScanningAlarmManager;
import com.iralauf.investment.coravita.databinding.ActivityMainBinding;
import com.iralauf.investment.coravita.models.AlertMode;
import com.iralauf.investment.coravita.persistence.AppDatabaseClient;
import com.iralauf.investment.coravita.utils.Common;
import com.iralauf.investment.coravita.utils.GPSUtils;
import com.iralauf.investment.coravita.utils.SharedPrefUtils;
import com.iralauf.investment.coravita.views.submodules.AppSettingsActivity;
import com.iralauf.investment.coravita.views.submodules.MoreInfoActivity;
import com.iralauf.investment.coravita.views.submodules.StatisticsActivity;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import lucifer.org.snackbartest.Icon;
import lucifer.org.snackbartest.MySnack;

import static com.iralauf.investment.coravita.utils.Common.errorLogger;
import static com.iralauf.investment.coravita.utils.Common.setUpTheme;
import static com.iralauf.investment.coravita.utils.GPSUtils.GPS_REQUEST;


public class MainActivity extends AppCompatActivity {

    /* ********************************************************************************************** */
    public static final String TAG = MainActivity.class.getSimpleName();
    //Bluetooth
    private BluetoothAdapter bAdapter=null;
    private ActivityMainBinding activityMainBinding;
    //Location
    private FusedLocationProviderClient fusedLocationClient;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private SharedPrefUtils sharedPrefUtils;
    private ActiveModeScanningAlarmManager activeModeScanningAlarmManager;
    /* ********************************************************************************************** */

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            activityMainBinding = ActivityMainBinding.inflate(getLayoutInflater());
            View view = activityMainBinding.getRoot();
            setUpTheme(this);
            setContentView(view);
            sharedPrefUtils = SharedPrefUtils.getInstance(this);
            initiate();
        } catch (Exception e) {
            errorLogger(e, "onCreate: ", TAG);
        }
    }

    /* ********************************************************************************************** */

    private void initiate() {
        try {
            if (!sharedPrefUtils.getNetworkAvailability()) Common.showCustomDialog(this, "Information", getString(R.string.internet_not_available), AlertMode.WARNING);

            InitiateGPSAndRequestLocation();//Check the gps connection
            InitiateBluetoothEnabled();//Check the bluetooth connection
            checkIsActive();//Check the scan status
            loadRiskValue();
        } catch (Exception e) {
            errorLogger(e, e.getMessage(), TAG);
        }
    }
    /* ********************************************************************************************** */

    private void loadRiskValue() {

        Handler handler = new Handler(Looper.myLooper()) {
            @Override
            public void handleMessage(Message msg) {
                Bundle bundle = msg.getData();
                double riskValue = bundle.getDouble("riskValue");
                activityMainBinding.riskvalue.setText(String.valueOf(riskValue));
                updateRiskText(riskValue);
            }
        };

        Runnable runnable = () -> {
            Message msg = handler.obtainMessage();
            Double riskValue=AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().riskDao().getLastRiskValue();
            if(riskValue!=null) {
                Bundle bundle = new Bundle();
                bundle.putDouble("riskValue", Double.parseDouble(Common.convertRiskDecimals(riskValue)));
                msg.setData(bundle);
                handler.sendMessage(msg);
            }
        };
        Thread riskThread = new Thread(runnable);
        riskThread.start();
    }

    private void updateRiskText(double riskValue) {

        if(riskValue>=0.0 && riskValue<=0.2){
            activityMainBinding.riskText.setText(getText(R.string.low_risk_sl));
            activityMainBinding.riskBackground.setImageResource(R.drawable.risk_low);
            activityMainBinding.coraaColorScale.lowriskLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.selected_aqi_foreground));
        }else if(riskValue>=0.2 && riskValue<=0.5){
            activityMainBinding.riskText.setText(getText(R.string.moderate_risk_sl));
            activityMainBinding.riskBackground.setImageResource(R.drawable.risk_moderate);
            activityMainBinding.coraaColorScale.moderateriskLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.selected_aqi_foreground));
        }else if(riskValue>=0.5 && riskValue<=0.9){
            activityMainBinding.riskText.setText(getText(R.string.high_risk_sl));
            activityMainBinding.riskBackground.setImageResource(R.drawable.risk_high);
            activityMainBinding.coraaColorScale.hihgriskLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.selected_aqi_foreground));
        }else if(riskValue>=0.9 && riskValue<=1.0){
            activityMainBinding.riskText.setText(getText(R.string.veryhigh_risk_sl));
            activityMainBinding.riskBackground.setImageResource(R.drawable.risk_veryhigh);
            activityMainBinding.coraaColorScale.veryhighriskLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.selected_aqi_foreground));
        }
    }

    /* ********************************************************************************************** */
    private void checkIsActive() {
        activeModeScanningAlarmManager=new ActiveModeScanningAlarmManager();
        activeModeScanningAlarmManager.initActiveMode(this);
        if (sharedPrefUtils.getScanningMode()) {
            enableScanButton();
            activeModeScanningAlarmManager.startActiveMode();
            activityMainBinding.templogger.append("Scan is in ACTIVE Mode..");
        }else{
            disableScanButton();
            disablePauseButton();
            activeModeScanningAlarmManager.stopActiveMode();
            activityMainBinding.templogger.append("Scan is in PAUSE Mode..");
            //TODO start the PAUSE MODE service to duplicate data
        }
    }

    private void enableScanButton(){
        activityMainBinding.enablescan.setOn(true);
        activityMainBinding.enablescan.setColorOn(getResources().getColor(R.color.green_500));
    }

    private void disableScanButton(){
        activityMainBinding.enablescan.setOn(false);
        activityMainBinding.enablescan.setColorOn(getResources().getColor(R.color.red_500));
        activityMainBinding.enablescan.setEnabled(false);
    }

    private void disablePauseButton() {
        activityMainBinding.enablescan.postDelayed(() -> {
            activityMainBinding.enablescan.setEnabled(true);
            activityMainBinding.enablescan.invalidate();
        }, 5000);
    }

    /* ********************************************************************************************** */

    @SuppressLint("MissingPermission")
    private void InitiateGPSAndRequestLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(Common.LOCATION_INTERVAL);
        LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        sharedPrefUtils.setCurrentLatitude(location.getLatitude());
                        sharedPrefUtils.setCurrentLongitude(location.getLongitude());
                        Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
                        try {
                            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                            String currentAddress = String.format("%s,%s", addresses.get(0).getLocality(), addresses.get(0).getCountryName());
                            activityMainBinding.locationTextView.setText(currentAddress);
                            sharedPrefUtils.setCurrentCountry(addresses.get(0).getCountryName());
                            sharedPrefUtils.setCurrentCity(addresses.get(0).getLocality());
                            Log.e(TAG, "onLocationResult: "+ sharedPrefUtils.getCurrentCountry());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        activityMainBinding.locationTextView.setText(R.string.enable_gps_forinfo);
                    }
                }
            }
        };

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
        } else {
            new GPSUtils(this).turnGPSOn();
        }
    }
    /* ********************************************************************************************** */

    private void InitiateBluetoothEnabled() {
        bAdapter = BluetoothAdapter.getDefaultAdapter();
        if(bAdapter == null)
        {
            Toast.makeText(getApplicationContext(),R.string.nobluetoothsupport,Toast.LENGTH_SHORT).show();
        }
        else{
            if(!bAdapter.isEnabled()){
                bAdapter.enable();
            }
        }
    }
    /* ********************************************************************************************** */

    public void activateScanning(View view){
        activityMainBinding.enablescan.setOnToggledListener((toggleableView, isOn) -> {
            sharedPrefUtils.setScanningMode(isOn);
            checkIsActive();
        });

    }

    /* ********************************************************************************************** */
    @Override
    public void onBackPressed() {

        new MySnack.SnackBuilder(this)
                .setText(getResources().getString(R.string.are_you_sure_want_to_exit))
                .setTextColor("#ffffff")
                .setTextSize(20)
                .setBgColor("#000000")
                .setDurationInSeconds(10)
                .setActionBtnColor("#D50000")
                .setIcon(Icon.WARNING)
                .setIcon(R.drawable.ic_close_black_24dp)
                .setActionListener(getString(R.string.yes), view -> {
                    finishAffinity();
                    System.gc();
                })
                .build();

    }
    /* ********************************************************************************************** */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {// If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    //Request location updates:
                    InitiateGPSAndRequestLocation();
                }
            }
        }
    }
    /* ********************************************************************************************** */
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GPS_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                InitiateGPSAndRequestLocation();
            }
        }
    }

    /* ********************************************************************************************** */

    public void updateTheme(View view){
        SharedPrefUtils sharedPrefUtils = SharedPrefUtils.getInstance(this);
        sharedPrefUtils.isDarkMode(!sharedPrefUtils.isDarkMode());
        recreate();
    }

    public void performRefresh(View view){
        Common.showToast("Refresh", Toast.LENGTH_SHORT, view.getContext());
        recreate();
    }

    public void updateLanguage(View view){
        Common.showToast("Language change", Toast.LENGTH_SHORT, view.getContext());
    }

    public void showStatistics(View view){
        startActivity(new Intent(this, StatisticsActivity.class));
    }

    public void showSettings(View view){
        startActivity(new Intent(this, AppSettingsActivity.class));
    }

    public void showMoreInfo(View view){
        startActivity(new Intent(this, MoreInfoActivity.class));
    }


    /* ********************************************************************************************** */

    @Override
    protected void onStop() {
        super.onStop();
        System.gc();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }

    /* ********************************************************************************************** */

}