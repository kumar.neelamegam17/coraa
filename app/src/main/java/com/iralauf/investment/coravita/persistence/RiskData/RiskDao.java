package com.iralauf.investment.coravita.persistence.RiskData;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface RiskDao {
    @Query("SELECT * FROM RiskObject")
    List<RiskObject> getAll();

    @Insert
    void insert(RiskObject task);

    @Delete
    void delete(RiskObject task);

    @Update
    void update(RiskObject task);

    @Query("SELECT riskValue FROM RiskObject WHERE Id = (SELECT MAX(Id)  FROM RiskObject);")
    double getLastRiskValue();
}
