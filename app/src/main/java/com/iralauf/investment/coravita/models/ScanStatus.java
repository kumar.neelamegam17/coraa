package com.iralauf.investment.coravita.models;

public enum ScanStatus {
    SCANNED, DUPLICATE
}
