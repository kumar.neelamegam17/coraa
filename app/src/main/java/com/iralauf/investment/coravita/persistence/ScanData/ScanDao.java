package com.iralauf.investment.coravita.persistence.ScanData;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ScanDao {
    @Query("SELECT * FROM ScanObject WHERE isactive=1")
    List<ScanObject> getAll();

    @Query("SELECT COUNT(DISTINCT mac_address) as count FROM ScanObject WHERE  countTag=:countTag AND isactive=1")
    int getDeviceCount(int countTag);

    @Insert
    void insert(ScanObject task);

    @Delete
    void delete(ScanObject task);

    @Update
    void update(ScanObject task);

    @Query("UPDATE ScanObject set isactive=0 and countTag=0 WHERE isactive=1")
    void updateAllActiveStatus();

    @Query("SELECT SUM(t.count) as count FROM (SELECT count(DISTINCT mac_address)as count FROM ScanObject WHERE isactive=1 GROUP BY mac_address HAVING COUNT(mac_address) >  :uniqueDeviceCount) as t")
    int getTotalDeviceCountInLastScan(int uniqueDeviceCount);

}
