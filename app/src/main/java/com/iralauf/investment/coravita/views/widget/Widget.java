package com.iralauf.investment.coravita.views.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.widget.RemoteViews;

import androidx.work.WorkManager;

import com.iralauf.investment.coravita.R;
import com.iralauf.investment.coravita.utils.SharedPrefUtils;
import com.iralauf.investment.coravita.views.coremodules.MainActivity;

/**
 * Implementation of App Widget functionality.
 */
public class Widget extends AppWidgetProvider {

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        try {
            // Construct the RemoteViews object
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_aqi);
            String sharedRiskValue = SharedPrefUtils.getInstance(context).getLastRiskValue();
            views.setTextViewText(R.id.widget_aqi_text, sharedRiskValue);
            double riskValue = Double.parseDouble(sharedRiskValue);
            CharSequence label = "--";
            if(riskValue>=0.0 && riskValue<=0.2){
                label = context.getString(R.string.low_risk_sl);
                views.setImageViewResource(R.id.widget_background, R.drawable.widget_circle_good);
            }else if(riskValue>=0.2 && riskValue<=0.5){
                label = context.getString(R.string.moderate_risk_sl);
                views.setImageViewResource(R.id.widget_background, R.drawable.widget_circle_moderate);
            }else if(riskValue>=0.5 && riskValue<=0.9){
                label = context.getString(R.string.high_risk_sl);
                views.setImageViewResource(R.id.widget_background, R.drawable.widget_circle_unhealthysg);
            }else if(riskValue>=0.9 && riskValue<=1.0){
                label = context.getString(R.string.veryhigh_risk_sl);
                views.setImageViewResource(R.id.widget_background, R.drawable.widget_circle_unhealthy);
            }

            views.setTextViewText(R.id.widget_risk_quality_text, label);
            views.setTextColor(R.id.widget_risk_quality_text, context.getResources().getColor(R.color.white));
            Intent intent = new Intent(context, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
            views.setOnClickPendingIntent(R.id.widget_background, pendingIntent);
            appWidgetManager.updateAppWidget(appWidgetId, views);
        } catch (NumberFormatException | Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
        WorkManager.getInstance().cancelAllWork();
    }
}

