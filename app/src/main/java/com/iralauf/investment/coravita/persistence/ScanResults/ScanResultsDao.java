package com.iralauf.investment.coravita.persistence.ScanResults;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ScanResultsDao {
    @Query("SELECT * FROM ScanResultsObject WHERE isactive=1")
    List<ScanResultsObject> getAll();

    @Insert
    void insert(ScanResultsObject task);

    @Delete
    void delete(ScanResultsObject task);

    @Update
    void update(ScanResultsObject task);

    @Query("UPDATE ScanResultsObject set isactive=0 WHERE isactive=1")
    void updateActiveStatus();

    @Query("SELECT SUM(deviceCount) as DEVICECOUNT FROM ScanResultsObject WHERE isactive=1")
    int getAllDeviceCount();
}
