# Coravita

Calculation of COVID risk due to 'close contacts'

CORAVITA stands for 
************
COVID for risk to be managed 
RA for Risk Assessment
VITA for life.

About
*******
CORAVITA … autonomous, all the time, everywhere
• COVID Risk Assessment Application
• Personal COVID Management
• Location independent
• Device Independent
• Application independent
• Cooperation independent

Features 
**********
Application and data resides exclusively on user’s device.
Works with any device with active BT, globally.
Adds public COVID information (e.g. positivity).
Documents individual COVID risk profile.
Offers display and alert functionality.
Acts as hub to other relevant web sites.

Project Concept and Support:
*******************************
Peter Sommerer
coravita@erlauf.com

Developer 
***********
Kumar Neelamegam
coravita@erlauf.com
https://aboutkumar.web.app/
https://play.google.com/store/apps/dev?id=8022266258048212480

Support
*********
coravita@erlauf.com
